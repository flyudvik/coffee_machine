/*
 *
 * BuyButton constants
 *
 */

export const DEFAULT_ACTION = 'app/BuyButton/DEFAULT_ACTION';
export const BUY_BUTTON_CLICK = 'app/BuyButton/BUY_BUTTON_CLICK';
