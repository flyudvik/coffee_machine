/*
 *
 * BuyButton
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { Button } from 'elemental';
import { map } from 'lodash';
import { bindActionCreators } from 'redux';
import * as actionCreators from './actions';
import { makeBuyButtonActive, makeSelectChange, makeSelectChangeCoins } from './selectors';

export class BuyButton extends React.Component { // eslint-disable-line react/prefer-stateless-function
  renderChangeCoins() {
    const { changeCoins } = this.props;
    return (
      <div>
        {map(changeCoins, (quantity, nominal) => (
          <p key={nominal}>
            {nominal} : {quantity}
          </p>
        ))}
      </div>
    );
  }
  render() {
    const { active, change } = this.props;
    const { buyButtonClick } = this.props.actions;
    if (active) {
      return (
        <div>
          <Button
            type="primary"
            onClick={() => buyButtonClick()}
          >
            Buy
          </Button>
          <p><b>Change:</b> {change}</p>
          <h3>Returned: </h3>
          {this.renderChangeCoins()}
        </div>
      );
    }
    return (
      <div>
        Need more gold, you owe {-change}
      </div>
    );
  }
}

BuyButton.propTypes = {
  actions: PropTypes.object.isRequired,
  active: PropTypes.bool,
  change: PropTypes.number,
  changeCoins: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  active: makeBuyButtonActive(),
  change: makeSelectChange(),
  changeCoins: makeSelectChangeCoins(),
});

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(actionCreators, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(BuyButton);
