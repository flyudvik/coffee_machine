/*
 *
 * BuyButton reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION, BUY_BUTTON_CLICK,
} from './constants';

const initialState = fromJS({});

function buyButtonReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case BUY_BUTTON_CLICK:
      return state;
    default:
      return state;
  }
}

export default buyButtonReducer;
