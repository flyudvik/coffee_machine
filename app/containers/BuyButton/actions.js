/*
 *
 * BuyButton actions
 *
 */

import {
  DEFAULT_ACTION, BUY_BUTTON_CLICK,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}
export function buyButtonClick() {
  alert('Bought!');
  return {
    type: BUY_BUTTON_CLICK,
  };
}
