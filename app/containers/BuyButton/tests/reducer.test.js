
import { fromJS } from 'immutable';
import buyButtonReducer from '../reducer';

describe('buyButtonReducer', () => {
  it('returns the initial state', () => {
    expect(buyButtonReducer(undefined, {})).toEqual(fromJS({}));
  });
});
