import { createSelector } from 'reselect';
import { each, filter } from 'lodash';
import { makeSelectInputedSum } from '../CoinInput/selectors';
import { makeSelectSummary } from '../SelectedMix/selectors';

/**
 * Direct selector to the buyButton state domain
 */
const selectBuyButtonDomain = () => (state) => state.get('buyButton');

/**
 * Other specific selectors
 */

const makeBuyButtonActive = () => createSelector(
  makeSelectInputedSum(),
  makeSelectSummary(),
  (inputed, required) => (inputed >= required)
);

const makeSelectChange = () => createSelector(
  makeSelectInputedSum(),
  makeSelectSummary(),
  (inputed, required) => (inputed - required)
);

const makeSelectChangeCoins = () => createSelector(
  makeSelectChange(),
  (change) => {
    const coins = {};
    let ch = Number.parseInt(change);
    each([10, 5, 3, 1], (coin) => {
      const coinsNumber = Math.floor(ch / coin);
      coins[coin] = coinsNumber;
      ch -= coinsNumber * coin;
    });
    return coins;
  }
);

/**
 * Default selector used by BuyButton
 */

const makeSelectBuyButton = () => createSelector(
  selectBuyButtonDomain(),
  (substate) => substate.toJS()
);

export default makeSelectBuyButton;
export {
  selectBuyButtonDomain,
  makeBuyButtonActive,
  makeSelectChange,
  makeSelectChangeCoins,
};
