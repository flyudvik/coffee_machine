/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { Row, Col } from 'elemental';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import CoinInput from '../CoinInput';
import Storage from '../Storage';
import SelectedMix from '../SelectedMix';
import BuyButton from '../BuyButton/index';


export default class HomePage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div>
        <h1>
          <FormattedMessage {...messages.header} />
        </h1>
        <Row>
          <Col sm="1/3">
            <CoinInput />
            <SelectedMix />
            <BuyButton />
          </Col>
          <Col sm="2/3">
            <Storage />
          </Col>
        </Row>
      </div>
    );
  }
}
