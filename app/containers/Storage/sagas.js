import { call, put } from 'redux-saga/effects';
import { takeEvery } from 'redux-saga';
import { storageFetched, loadingFailed } from './actions';
import { STORAGE_FETCH_REQUESTED } from './constants';
import { getCurrentStorage } from '../../api';

// Individual exports for testing
function* fetchStorage() {
  try {
    const data = yield call(getCurrentStorage);
    yield put(storageFetched(data));
    // yield put({ type: STORAGE_FETCHED, data });
  } catch (e) {
    yield put(loadingFailed());
  }
}

function* fetchStorageRequested() {
  yield takeEvery(STORAGE_FETCH_REQUESTED, fetchStorage);
}

// All sagas to be loaded
export default [
  fetchStorageRequested,
];
