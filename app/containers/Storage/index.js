/*
 *
 * Storage
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { Row, Col, Card, Button } from 'elemental';
import { map } from 'lodash';
import { bindActionCreators } from 'redux';

import makeSelectStorage, { makeSelectStorageItems } from './selectors';
import * as actionCreators from './actions';

export class Storage extends React.Component {
  componentWillMount() {
    const { requestStorageFetch } = this.props.actions;
    requestStorageFetch();
  }

  render() {
    const { storageItems } = this.props;
    const { selectFromMenu } = this.props.actions;
    return (
      <div>
        <h2>Main drinks</h2>
        <Row>
          {map(storageItems, (storageItem, index) => (
            <Col key={index} sm="1/3">
              <Card>
                <div>
                  <h3>{storageItem.name}</h3>
                  <Button
                    block
                    type={storageItem.type === 'main' ? 'hollow-primary' : 'hollow-success'}
                    onClick={() => selectFromMenu(storageItem)}
                  >
                    {storageItem.price}
                  </Button>
                </div>
              </Card>
            </Col>
          ))}
        </Row>
      </div>
    );
  }
}

Storage.propTypes = {
  actions: PropTypes.object,
  storageItems: PropTypes.array,
};

const mapStateToProps = createStructuredSelector({
  Storage: makeSelectStorage(),
  storageItems: makeSelectStorageItems(),
});

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(actionCreators, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Storage);
