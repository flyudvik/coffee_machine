import { createSelector } from 'reselect';
import { filter } from 'lodash';

/**
 * Direct selector to the storage state domain
 */
const selectStorageDomain = () => (state) => state.get('storage');

/**
 * Other specific selectors
 */
const makeSelectStorageItems = () => createSelector(
  selectStorageDomain(),
  (substate) => filter(substate.toJS(), (obj) => (obj.quantity > 0))
);

/**
 * Default selector used by Storage
 */

const makeSelectStorage = () => createSelector(
  selectStorageDomain(),
  (substate) => substate.toJS()
);

export default makeSelectStorage;
export {
  selectStorageDomain,
  makeSelectStorageItems,
};
