/*
 *
 * Storage constants
 *
 */

export const DEFAULT_ACTION = 'app/Storage/DEFAULT_ACTION';
export const STORAGE_FETCH_REQUESTED = 'app/Storage/STORAGE_FETCH_REQUESTED';
export const STORAGE_FETCHED = 'app/Storage/STORAGE_FETCHED';
export const LOADING_FAILED = 'app/Storage/LOADING_FAILED';
export const SELECT_FROM_MENU = 'app/Storage/SELECT_FROM_MENU';
