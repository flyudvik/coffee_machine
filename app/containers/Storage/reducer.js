/*
 *
 * Storage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION, STORAGE_FETCHED,
} from './constants';

const initialState = fromJS({});

function storageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case STORAGE_FETCHED:
      return fromJS(action.data);
    default:
      return state;
  }
}

export default storageReducer;
