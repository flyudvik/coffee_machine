/*
 *
 * Storage actions
 *
 */

import {
  DEFAULT_ACTION, STORAGE_FETCHED, LOADING_FAILED, STORAGE_FETCH_REQUESTED, SELECT_FROM_MENU,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function storageFetched(data) {
  return {
    type: STORAGE_FETCHED,
    data,
  };
}

export function loadingFailed(error) {
  return {
    type: LOADING_FAILED,
    error,
  };
}

export function selectFromMenu(sort) {
  return {
    type: SELECT_FROM_MENU,
    payload: {
      name: sort.name,
      price: sort.price,
      type: sort.type,
    },
  };
}


export function requestStorageFetch() {
  return {
    type: STORAGE_FETCH_REQUESTED,
  };
}
