
import { fromJS } from 'immutable';
import storageReducer from '../reducer';

describe('storageReducer', () => {
  it('returns the initial state', () => {
    expect(storageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
