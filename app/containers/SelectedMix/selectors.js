import { createSelector } from 'reselect';
import { sum, map } from 'lodash';

/**
 * Direct selector to the selectedMix state domain
 */
const selectSelectedMixDomain = () => (state) => state.get('selectedMix');

/**
 * Other specific selectors
 */
const makeSelectSummary = () => createSelector(
  selectSelectedMixDomain(),
  (substate) => (
    sum(map(substate.toJS(), (value) => (value.price)))
  )
);

/**
 * Default selector used by SelectedMix
 */

const makeSelectSelectedMix = () => createSelector(
  selectSelectedMixDomain(),
  (substate) => substate.toJS()
);

export default makeSelectSelectedMix;
export {
  selectSelectedMixDomain,
  makeSelectSummary,
};
