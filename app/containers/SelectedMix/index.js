/*
 *
 * SelectedMix
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { Pill } from 'elemental';
import { map } from 'lodash';
import { bindActionCreators } from 'redux';
import makeSelectSelectedMix, { makeSelectSummary } from './selectors';
import * as actionCreators from './actions';

export class SelectedMix extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { selections, summary } = this.props;
    const { removeSelection } = this.props.actions;
    return (
      <div>
        <div>
          {map(selections, (value, key) => (
            <Pill
              key={key}
              label={value.name}
              onClear={() => removeSelection(value)}
              type={value.type === 'main' ? 'primary' : 'success'}
            />
          ))}
        </div>
        <div>
          <p><b>Needed sum: </b>{summary}</p>
        </div>
      </div>
    );
  }
}

SelectedMix.propTypes = {
  actions: PropTypes.object,
  selections: PropTypes.array,
  summary: PropTypes.number,
};

const mapStateToProps = createStructuredSelector({
  selections: makeSelectSelectedMix(),
  summary: makeSelectSummary(),
});

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(actionCreators, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(SelectedMix);
