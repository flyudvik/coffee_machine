/*
 *
 * SelectedMix reducer
 *
 */

import { OrderedSet } from 'immutable';
import {
  DEFAULT_ACTION, REMOVE_SELECTION,
} from './constants';
import {
  SELECT_FROM_MENU,
} from '../Storage/constants';

const initialState = OrderedSet();

function selectedMixReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case SELECT_FROM_MENU:
      return state.add(action.payload);
    case REMOVE_SELECTION:
      return state.delete(action.payload);
    default:
      return state;
  }
}

export default selectedMixReducer;
