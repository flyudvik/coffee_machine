/*
 *
 * SelectedMix constants
 *
 */

export const DEFAULT_ACTION = 'app/SelectedMix/DEFAULT_ACTION';
export const REMOVE_SELECTION = 'app/SelectedMix/REMOVE_SELECTION';
