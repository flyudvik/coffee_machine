
import { fromJS } from 'immutable';
import selectedMixReducer from '../reducer';

describe('selectedMixReducer', () => {
  it('returns the initial state', () => {
    expect(selectedMixReducer(undefined, {})).toEqual(fromJS({}));
  });
});
