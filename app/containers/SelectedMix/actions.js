/*
 *
 * SelectedMix actions
 *
 */

import {
  DEFAULT_ACTION, REMOVE_SELECTION,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function removeSelection(selected) {
  return {
    type: REMOVE_SELECTION,
    payload: selected,
  };
}
