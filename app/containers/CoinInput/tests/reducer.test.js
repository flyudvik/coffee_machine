
import { fromJS } from 'immutable';
import coinInputReducer from '../reducer';

describe('coinInputReducer', () => {
  it('returns the initial state', () => {
    expect(coinInputReducer(undefined, {})).toEqual(fromJS({}));
  });
});
