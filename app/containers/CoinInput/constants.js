/*
 *
 * CoinInput constants
 *
 */

export const DEFAULT_ACTION = 'app/CoinInput/DEFAULT_ACTION';
export const COIN_INPUT_CLICK = 'app/CoinInput/COIN_INPUT_CLICK';
