/*
 *
 * CoinInput reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION, COIN_INPUT_CLICK,
} from './constants';

const initialState = fromJS({
  10: 0,
  5: 0,
  3: 0,
  1: 0,
});

function coinInputReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case COIN_INPUT_CLICK:
      return state.set(action.nominal, state.get(action.nominal) + 1);
    default:
      return state;
  }
}

export default coinInputReducer;
