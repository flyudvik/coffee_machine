import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { ButtonGroup, Button } from 'elemental';
import { map } from 'lodash';
import { bindActionCreators } from 'redux';

import makeSelectCoinInput, { makeSelectInputedSum } from './selectors';
import * as actionCreators from './actions';


export class CoinInput extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { coins, summary } = this.props;
    const { coinInputClick } = this.props.actions;
    return (
      <div>
        <ButtonGroup>
          {map(coins, (quantity, nominal) => (
            <Button type="hollow-primary" key={nominal} onClick={() => coinInputClick(nominal)}>
              {nominal} : {quantity}
            </Button>
          ))}
        </ButtonGroup>
        <p><b>Summary: </b>{summary}</p>
      </div>
    );
  }
}

CoinInput.propTypes = {
  actions: PropTypes.object.isRequired,
  coins: PropTypes.object.isRequired,
  summary: PropTypes.number.isRequired,
};

const mapStateToProps = createStructuredSelector({
  coins: makeSelectCoinInput(),
  summary: makeSelectInputedSum(),
});

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(actionCreators, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(CoinInput);
