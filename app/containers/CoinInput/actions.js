/*
 *
 * CoinInput actions
 *
 */

import {
  DEFAULT_ACTION, COIN_INPUT_CLICK,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function coinInputClick(nominal) {
  return {
    type: COIN_INPUT_CLICK, nominal,
  };
}
