import { createSelector } from 'reselect';
import { sum, map } from 'lodash';

/**
 * Direct selector to the coinInput state domain
 */
const selectCoinInputDomain = () => (state) => state.get('coinInput');

/**
 * Other specific selectors
 */
const makeSelectInputedSum = () => createSelector(
  selectCoinInputDomain(),
  (state) => (
    sum(map(state.toJS(), (value, key) => (key * value)))
  )
);

/**
 * Default selector used by CoinInput
 */

const makeSelectCoinInput = () => createSelector(
  selectCoinInputDomain(),
  (substate) => substate.toJS()
);

export default makeSelectCoinInput;
export {
  selectCoinInputDomain,
  makeSelectInputedSum,
};
